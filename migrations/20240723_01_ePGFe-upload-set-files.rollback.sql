-- upload_set files
-- depends: 20240715_01_Hca9V-upload-set-metadata

DROP TABLE files;
DROP TYPE file_type;

DROP INDEX pictures_upload_set_idx;
