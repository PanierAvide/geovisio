-- collaborative_metadata_editing
-- depends: 20250102_01_EElhA-rm-cameras  20250107_01_EQN9v-tags-tables


ALTER TABLE accounts DROP COLUMN collaborative_metadata;

DROP TABLE configurations;