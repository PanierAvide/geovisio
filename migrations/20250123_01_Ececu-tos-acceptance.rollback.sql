-- tos_acceptance
-- depends: 20250109_01_4OOP4-pages  20250114_01_ABaaL-collaborative-metadata-editing

ALTER TABLE accounts DROP COLUMN tos_accepted;
ALTER TABLE accounts DROP COLUMN tos_accepted_at;
