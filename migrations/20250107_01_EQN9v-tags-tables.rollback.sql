-- tags_tables
-- depends: 20241224_01_xuN6n-delete-upload-set-on-last-picture-trg-statement

DROP TABLE annotations_semantics_history;
DROP TABLE sequences_semantics_history;
DROP TABLE pictures_semantics_history;
DROP TABLE annotations_semantics;
DROP TABLE annotations;
DROP TABLE sequences_semantics;
DROP TABLE pictures_semantics;